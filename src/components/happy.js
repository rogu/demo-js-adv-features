export class HappyComponent extends HTMLElement {

    constructor() {
        super();
        this.attachShadow({ mode: 'open' });

        this.shadowRoot.innerHTML = `
            <style>
                .box {
                    border: 2px solid;
                    font-size:50px;
                    padding: 0px;
                    text-align: center;
                    cursor: pointer;
                }
            </style>
            <div class="box">
                <span class="name">...</span>
                <div style="font-size:20px;">click me</div>
                <span class="happy">...</span>
            </div>`
    }

    static get observedAttributes() {
        return ['name', 'happy'];
    }

    attributeChangedCallback(attr, old, value) {
        this.render(attr, value);
    }

    render(attr, value) {
        const target = this.shadowRoot.querySelector(`.${attr}`);
        switch (attr) {
            case 'happy':
                target.innerHTML = this.happy ? '&#x1f603;' : '&#x1f620;'
                break;
            case 'name':
                target.innerHTML = value;
        }
    }

    onClick() {
        this.happy = !this.happy;
    }

    get happy() {
        return this.hasAttribute('happy');
    }

    set happy(value) {
        value
            ? this.setAttribute('happy', true)
            : this.removeAttribute('happy');
    }

    connectedCallback() {
        this.addEventListener('click', this.onClick);
    }
}

customElements.define('app-happy', HappyComponent);
