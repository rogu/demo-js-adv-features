/**
 * Regular function
 * przekazywanie funkcji; wywołanie zwrotne funkcji
 */

function show() {
    console.log('click from global ', this);
}

document.body.addEventListener('click', show);

// mniej więcej tak wywoływana jest funkcja show z poziomu funkcji addEventLisener
show.call(document.body);

/**
 * Regular function w obiekcie
 */

const obj1 = {
    listen() {
        document.body.addEventListener('click', function () {
            console.log('click from obj1', this);
        });
    }
}
obj1.listen();


/**
 * lambda function
 * Uruchomienie globalne
 *
 */

document.body.addEventListener('dblclick', () => {
    console.log('2click (lambda) from global ', this);
});


/**
 * Uruchomienie w kontekscie obiektu
 */

const obj2 = {
    listen() {
        document.body.addEventListener('dblclick', () => {
            console.log('2click (lambda) from obj2 ', this);
        });
    }
}
obj2.listen();


