import { ui } from "../utils/helpers.js";

/**
 * Promise
 */

export const getData = (time) => {
    return new Promise((res, rej) => {
        setTimeout(() => {
            time > 500 ? res(Date.now()) : rej('any error');
        }, time);
    })
}

export function getDataFromServer() {
    return fetch('https://api.debugger.pl/workers')
        .then((resp) => resp.json());
}

const promiseCard = ui.createCard('promise | async', 'yellow');

/**
 * Handling result with "then" function
 */

getDataFromServer().then(({ data }) => promiseCard.display('data from server', data.slice(0, 3)));

getData(1500)
    .then((value) => promiseCard.display('success data', value))
    .catch((err) => console.warn(err));

Promise
    .all([getData(1000), getData(2000), getData(3000)])
    .then((value) => promiseCard.display('all success data', value));

Promise
    .allSettled([getData(1000), getData(2000), Promise.reject('ups')])
    .then((value) => promiseCard.display('allSettled data', value));


/**
* Handling result with "async await"
*/

async function asyncFn() {
    const data = await getData(1000);
    promiseCard.display('async await data', data);
    try {
        const data = await getData(100);
        promiseCard.display('promise data', data);
    } catch (error) {
        promiseCard.display('promise error', error);
    }
}

asyncFn();


/**
 * when array map return promise
 */
(async () => {
    const userIds = [1, 2];
    const url = 'https://reqres.in/api/products/';
    const extraData = userIds.map(async (value) => fetch(`${url}${value}`).then((resp) => resp.json()));
    const withExtraData = await Promise.all(extraData);
    withExtraData.forEach((person, id) => promiseCard.display(`person${id}`, person.data, 'green'));
})()

