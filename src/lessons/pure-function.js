import { ui } from "../utils/helpers.js";

/**
 * Zalety używania pure function:
 * Given the same input, will always return the same output.
 * Produces no side effects
 * stateless
 * declarative
 * https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-pure-function-d1c076bec976
 */

const addToCart = (cart, item, quantity) => {
    return [...cart, {
        item,
        quantity
    }];
};

const items = [{ item: 'potatoes', quantity: 2 }];
ui
    .createCard('pure function')
    .display('add item to cart', addToCart(items, 'tomato', 5))
