import { ui } from "../utils/helpers.js";

/**
* Template tag
*/
const tpl = document.querySelector('#tpl-item').content.cloneNode(true);
tpl.querySelector('.title').innerText = 'tomato'
tpl.querySelector('.image').setAttribute('src', 'https://api.debugger.pl/assets/tomato.jpg');
ui.createCard('html template tag').body.appendChild(tpl);


/**
 * es6 Template strings
 * interpolation
 */
const person = {
    name: 'Bob',
    languages: ['pl', 'en']
}
const card = ui.createCard('Template strings');
card.display('join string and variables', `my name is: ${person.name}`)
card.display('loop languages', `${person.languages.map((value) => value)}`);


/**
 * custom interpolation
 */

function quux(strings, ...values) {
    return { strings, values }
}
const xx = quux`foo\n${42}bar`
card.display('custom intrepolation', xx);

