
/**
 * Web components
 */

import { HappyComponent } from "../components/happy.js";

const webCCard = document.querySelector('.webc');
const happy = new HappyComponent();
happy.setAttribute('name', 'Bob');
webCCard.appendChild(happy);
