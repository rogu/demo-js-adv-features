import { ui } from "../utils/helpers.js";

/**
 * Map
 */
const mapCard = ui.createCard('map');
const itemTpl = document.querySelector('#tpl-item').content;
const contactTpl = document.querySelector('#tpl-contact').content;
let map = new Map();
map.set({ title: 'pumpkin', image: 'pumpkin.jpg' }, itemTpl.cloneNode(true));
map.set({ title: 'pepper', image: 'pepper.jpg' }, itemTpl.cloneNode(true));
map.set({ name: 'joe', phone: 1234 }, contactTpl.cloneNode(true));

for (const [data, node] of map) {
    for (const [key, value] of Object.entries(data)) {
        const el = node.querySelector(`.${key}`);
        switch (el.tagName) {
            case 'INPUT':
                el.value = value;
                break;
            case 'IMG':
                el.src = `https://api.debugger.pl/assets/${value}`;
                break;
            default:
                el.innerText = value;
        }
    }
    mapCard.body.appendChild(node);
}
