import { ui } from "../utils/helpers.js";

/**
 * Example 1
 * Generatory można zatrzymywać i wznawiać poleceniem yield
 * Fibonacci generator
 */
function* fibonacci(counter = 10) {
    let [current, next] = [0, 1];
    while (counter--) {
        yield current;
        [current, next] = [next, current + next]
    }
}

let first10 = fibonacci(10);
const card = ui.createCard('Generators');
card.display('fibonacci', [...first10]);
/* for (const iterator of first10) {
    console.log(iterator);
}*/



/**
 * Throttling
 */

export function* throttle(func, time) {
    let lastDate = Date.now();
    function throttled(date) {
        if (date >= lastDate + time) {
            lastDate = Date.now();
            func.bind(window)(date);
        }
    }
    while (true) throttled(yield);
}

const thr = throttle(console.log, 1111);
document.body.addEventListener('mousemove', () => thr.next(Date.now()));

