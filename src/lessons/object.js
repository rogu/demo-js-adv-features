import { ui } from "../utils/helpers.js";

/**
 * Object
 */

const obj = { id: 1, name: 'joe' };
const card = ui.createCard('Object');
card.display('object keys', Object.keys(obj))
card.display('object values', Object.values(obj))
card.display('object entries', Object.entries(obj));


/**
 * defineProperty
 */

Object.defineProperty(obj, 'secret', { value: 'super secret', enumerable: false })
card.display('defineProperty',
    `secret is unavailable <br>${Object.keys(obj).map((key) => `${key} is available <br>`).join('')}`);

/**
 * freeze
 */
const Colors = Object.freeze({
    RED: Symbol("red"),
    BLUE: Symbol("blue"),
    GREEN: Symbol("green")
});
//Colors.BLUE = 'nie zmienisz na niebieski';
card.display('freeze colors', Colors);
