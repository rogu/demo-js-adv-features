import { ui } from "../utils/helpers.js";

/**
 * Rest params
 */

export function format(str, ...args) {
    return str.replace(/{(\d)}/g, (value, idx) => {
        return args[idx];
    })
}

let msg = format(
    'The {0}st arg is a string, the {1} are {2}.',
    1,
    'rest',
    'unknown');
ui
    .createCard('rest params')
    .display('formatted', msg);

