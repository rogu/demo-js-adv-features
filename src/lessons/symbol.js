//https://medium.com/intrinsic/javascript-symbols-but-why-6b02768f4a5c

import { ui } from "../utils/helpers.js";

const secret = Symbol();

export class Auth {

    constructor() {
        Object.defineProperty(this, "access", {
            get: () => this[secret]
        });
    }
    async login(cb) {
        const data = await fetch('https://auth.debugger.pl/login',
            {
                method: 'POST',
                body: JSON.stringify({ username: 'admin@localhost', password: 'Admin1' }),
                headers: { 'Content-Type': 'application/json' },
                credentials: 'include'
            });
        this[secret] = await data.json();
        cb();
    }
}

/**
 * es6 Symbol
 * read only property example
 */
const auth = new Auth(); // you can set access only by function login
auth.login(() => ui.createCard('symbol | async', 'yellow').display('auth login result:', auth.access));


