var obj = { value: 4 };

function foo(passedObj) {
    passedObj.value = 5;
}

console.log(obj.value);
foo(obj);
console.log(obj.value);
