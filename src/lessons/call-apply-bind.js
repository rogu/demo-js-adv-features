import { ui } from "../utils/helpers.js";
import { collection } from "../assets/data.js";

/**
 * call, apply, bind
 */

// funkcja getParams nie jest powiązana (jeszcze) z żadnym obiektem
function getParams(...params) {
    let result = {};
    for (let param of params) {
        result[param] = this[param];
    }
    return result;
}

/**
 * call - wywołanie funkcji w kontekscie obiektu przekazanego w 1-szym parametrze.
 * Argumenty, które chcemy przekazać do wykonywnej funkcji podajemy po przecinkach.
 */
const callExample = getParams.call(collection[1], 'name', 'phone');

/**
 * apply - działa b. podobnie do call z tą różnicą, że argumenty, które chcemy przekazać do wykonywnej funkcji podajemy w tablicy.
 */
const applyExample = getParams.apply(collection[1], ['name', 'phone', 'category']);

/**
 * bind - podobnie jak call, z tą różnicą, że zwraca funkcję którą można wykonać później.
 * Tworzy nową funkcję której kontekstem jest obiekt przekazany do funkcji bind.
 */
const bindExample = getParams.bind(collection[1])('name');

ui
    .createCard('call, apply, bind')
    .display('call', callExample)
    .display('apply', applyExample)
    .display('bind', bindExample)
