/**
 * Scope - zakres dostępu do zmiennych
 * Lexical - określany jest w momencie definiowana kodu
 * Dynimic - określany jest w momencie wykonywania kodu
 */

 /**
  * W JavaScript mamy leksykalny scope
  */

function foo() {
    console.log('-------',a); // 2
}

function bar() {
    var a = 3;
    // wywołanie funkcji foo w kontekście bar nie spowoduje pobrania wartości zmiennej "a" z kontekstu wywołania
    foo();
}

var a = 2;

bar();


/**
 * Program korzysta ze zmiennych dostępnych leksykalnie
 */

function foo2(a) {

    var b = a * 2;

    function bar2(c) {
        console.log(a, b, c);
    }

    bar2(b * 3);
}

foo2(2); // 2 4 12
