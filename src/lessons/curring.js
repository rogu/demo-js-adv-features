import { ui } from "../utils/helpers.js";

/**
 * Curring
 * proces rozkładania funkcji przyjmującej wiele parametrów do zestawu funkcji które przyjmują tylko jeden argument
 */

function add(x, y) {
    if (typeof y === 'undefined') {
        return function (y) { // curring. aplikacja częściowa
            return x + y;
        }
    }
    return x + y; // aplikacja pełna
}

const addToTwo = add(2);
ui.createCard('Curring')
    .display('2 + 2 is', addToTwo(2))
    .display('2 + 4 is', add(2)(4))
    .display('2 + 8 is', addToTwo(8));

