import { ui } from "../utils/helpers.js";

/**
 * Closure
 * Domknięcie daje Ci dostęp z funkcji wewnętrznej do zasięgu funkcji zewnętrznej.
 */

export const debounce = (() => {
    let timer;
    // poniższa funkcja ma dostęp do zmiennej timer dzięki domknięciu
    return (cb, time = 500) => {
        clearTimeout(timer);
        timer = setTimeout(() => cb(), time);
    }
})();


const closureCard = ui.createCard('closure | async', 'yellow');
debounce(() => closureCard.display('debounce', 'it will be canceled'));
debounce(() => closureCard.display('debounce', 'it will be canceled'));
debounce(() => closureCard.display('debounce', 'eeeeee'));

