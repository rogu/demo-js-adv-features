import { ui } from "../utils/helpers.js";
import { collection } from "../assets/data.js";

/**
 * Reduce
 * Funkcja ma wiele zastosowań. Poniżej kilka prostych przykładów do analizy.
 */

const mapWithReduce = (fn, arr) => arr.reduce((acc, item) => [...acc, fn(item)], []);

const filterWithReduce = (fn, arr) => arr.reduce((acc, item) => fn(item) ? [...acc, item] : acc, []);

const arrayAsEntities = (arr) => arr.reduce((acc, item) => ({ ...acc, [item.id]: item }), {});

const objToQueryParams = (obj) =>
    Object.entries(obj).reduce((acc, [title, value], idx) =>
        `${acc}${idx === 0 ? '' : '&'}${title}=${value}`, '?');

const queryParamsToObj = (str) => {
    const arr = str.split('?')[1].split('&');
    return Object.fromEntries(arr.map((item) => item.split('=')));
}

const queryString = objToQueryParams({ title: 'tomato', priceFrom: 10 });

ui
    .createCard('Reduce')
    .display('get value from the collections in entities', arrayAsEntities(collection)["5b995ed97971b1b68b775cea"])
    .display('obj to query params', queryString)
    .display('query params to obj', queryParamsToObj('any' + queryString))
    .display('map with reduce', mapWithReduce((item) => item * 100, [1, 2, 3]))
    .display('filter with reduce', filterWithReduce((item) => item > 2, [1, 2, 3]))


