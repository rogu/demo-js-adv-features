import { ui } from "../utils/helpers.js";

/**
 * es6 spread
 */
const spreadCard = ui.createCard('spread');
const oryginalObj = { id: 1, name: 'joe' };
const copy = { ...oryginalObj };
spreadCard.display('copy object', copy);

const { id, ...withOutId } = copy;
spreadCard.display('copy without id', withOutId);
spreadCard.display('only id', id);

const conditionTrue = { ...copy, ...(true && { access: true }) };

spreadCard.display('condition true', conditionTrue);
const conditionFalse = { ...copy, ...(false && { access: true }) };
spreadCard.display('condition false', conditionFalse);

const oryginalArr = [{ name: "Joe" }, { name: "Bob" }, { name: "Mike" }];
const arrCopy = [...oryginalArr];
spreadCard.display('copy array', arrCopy);
const [first, second] = arrCopy;
const [, , last] = arrCopy;

const arrCondition = [1,2,3,4,5,6].reduce((acc, val) => [...acc, ...val % 2 === 0 ? [val] : []], [])
spreadCard.display('copy array with condition', arrCondition);

spreadCard.display('first array element', first);
spreadCard.display('second array element', second);
spreadCard.display('last array element', last);
