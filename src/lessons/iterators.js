import { ui } from "../utils/helpers.js";

/**
 * Iterators
 * Iterators vs generators https://stackoverflow.com/a/37124539/3733339
 */

const card = ui.createCard('Iterators');

/**
 * Example 0
 * Przykład pokazuje kto polecił kolejnego członka mafii
 */

function recommendationOrder(members) {
    return {
        [Symbol.iterator]() {
            let member = { next: members.find(i => i.head).id };
            let [done, value] = [false];
            return {
                next() {
                    member = members.find(i => i.id === member.next);
                    member
                        ? value = member.name
                        : done = true;
                    return { done, value };
                }
            };
        }
    }
}

const mafiaMembers = [
    { id: 'u1', name: 'Bob', next: 'u4', head: true },
    { id: 'u2', name: 'Kris', next: null },
    { id: 'u3', name: 'Mike', next: 'u2' },
    { id: 'u4', name: 'Joe', next: 'u3' }
];

const myMafia = recommendationOrder(mafiaMembers);

for (let item of myMafia)
    console.log(item);   // 'Bob', 'Joe', 'Mike', 'Kris'

card.display('mafia member recommendation', [...myMafia].join(' -> '))


/**
 * Example 1
 */
let fibonacci = {
    [Symbol.iterator]() {
        let pre = 0, cur = 1
        return {
            next() {
                [pre, cur] = [cur, pre + cur]
                return { done: false, value: cur }
            }
        }
    }
}

const fibonacciData = [];
for (let n of fibonacci) {
    if (n > 15) break;
    fibonacciData.push(n);
}
card.display('fibonacci', fibonacciData);

/**
 * Example 2
 */
function evenOnly(curr, max) {
    return {
        [Symbol.iterator]() {
            return {
                next() {
                    curr++;
                    if (curr % 2 === 0) {
                        return { done: false, value: curr }
                    } else if (curr >= max) {
                        return { done: true }
                    } else {
                        return this.next()
                    }
                }
            }
        }
    }
}

const evenOnlyData = [];
for (let ii of evenOnly(0, 10)) {
    evenOnlyData.push(ii);
}
card.display('ovenOnly', evenOnlyData);
