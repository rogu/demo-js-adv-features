export const ui = {
    count: 0,
    createCard(title, bgColor = 'white') {
        this.count++;
        const template = document.createElement('template');
        template.innerHTML = `
            <div class="card" style="background: ${bgColor}">
                <h4 class="card-header">${title} <span class="badge badge-secondary">${this.count}</span></h4>
                <div class="card-body"></div>
            </div>
        `;
        const content = template.content;
        const body = content.querySelector('.card-body');

        document.querySelector('.card-columns').appendChild(content);

        return {
            display(title, content, color = 'coral') {
                body.innerHTML += `<div style="background:${color}">
                    <div style="font-weight: bold;">${title}:</div>
                    <p>${typeof content === 'object' ? JSON.stringify(content) : content}</p>
                </div>`;
                return this;
            },
            body
        }
    }
};


